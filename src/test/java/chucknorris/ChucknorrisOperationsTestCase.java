package chucknorris;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import org.mule.functional.junit4.MuleArtifactFunctionalTestCase;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class ChucknorrisOperationsTestCase extends MuleArtifactFunctionalTestCase {

  /**
   * Specifies the mule config xml with the flows that are going to be executed in the tests, this file lives in the test resources.
   */
  @Override
  protected String getConfigFile() {
    return "test-mule-config.xml";
  }



  @Test
  public void executRandomOperation() throws Exception {
    String payloadValue = ((String) flowRunner("randomFlow")
                                      .run()
                                      .getMessage()
                                      .getPayload()
                                      .getValue());
    assertNotNull(payloadValue);
  }
}
