package chucknorris.internal;

import java.util.HashSet;
import java.util.Set;

import org.mule.runtime.extension.api.annotation.error.ErrorTypeProvider;
import org.mule.runtime.extension.api.error.ErrorTypeDefinition;

public class RandomOperationErrorTypeProvider implements ErrorTypeProvider {

    @Override
    public Set<ErrorTypeDefinition> getErrorTypes() {
        HashSet<ErrorTypeDefinition> errorTypes = new HashSet<>();
        errorTypes.add(ChucknorrisErrorType.CONNECTIVITY);
        errorTypes.add(ChucknorrisErrorType.TIMEOUT);
        errorTypes.add(ChucknorrisErrorType.UNSUPPORTED_CONTENT_TYPE);
        return errorTypes;
    }
}
