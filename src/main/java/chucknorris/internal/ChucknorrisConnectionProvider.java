package chucknorris.internal;

import javax.inject.Inject;

import org.mule.runtime.api.connection.CachedConnectionProvider;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.api.connection.ConnectionProvider;
import org.mule.runtime.api.connection.ConnectionValidationResult;
import org.mule.runtime.api.connection.PoolingConnectionProvider;
import org.mule.runtime.api.exception.MuleException;
import org.mule.runtime.api.lifecycle.Startable;
import org.mule.runtime.api.lifecycle.Stoppable;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.RefName;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.http.api.HttpService;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.client.HttpClientConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class (as it's name implies) provides connection instances and the
 * funcionality to disconnect and validate those connections.
 * <p>
 * All connection related parameters (values required in order to create a
 * connection) must be declared in the connection providers.
 * <p>
 * This particular example is a {@link PoolingConnectionProvider} which declares
 * that connections resolved by this provider will be pooled and reused. There
 * are other implementations like {@link CachedConnectionProvider} which lazily
 * creates and caches connections or simply {@link ConnectionProvider} if you
 * want a new connection each time something requires one.
 */
public class ChucknorrisConnectionProvider implements CachedConnectionProvider<ChucknorrisConnection>, Startable, Stoppable {

	private final Logger LOGGER = LoggerFactory.getLogger(ChucknorrisConnectionProvider.class);

    @ParameterGroup(name = "Proxy Config")
    ChucknorrisProxyConfig proxyConfig;
	
	@Parameter
	private String hostname;

	public String getHostname() {
		return hostname;
	}

	/**
	 * A parameter that is not required to be configured by the user.
	 */
	@DisplayName("TTL")
	@Parameter
	@Optional(defaultValue = "100")
	private int ttl;
	
	@Inject
	private HttpService httpService;

	@RefName
	private String configName;
	

	private HttpClient httpClient;
	

	@Override
	public ChucknorrisConnection connect() throws ConnectionException {
	    return new ChucknorrisConnection(hostname, httpClient);

	}



	@Override
	public ConnectionValidationResult validate(ChucknorrisConnection connection) {
		if (connection.isValid())
			return ConnectionValidationResult.success();
		else
			return ConnectionValidationResult.failure(configName, new Exception("Invalid connection"));
		
	}

	@Override
	public void stop() throws MuleException {
		httpClient.stop();

	}


	@Override
	public void start() throws MuleException {
		HttpClientConfiguration.Builder clientConfigurationBuilder = new HttpClientConfiguration.Builder().setName(configName);
		if (proxyConfig.getProxyConfig().isPresent()) {
			clientConfigurationBuilder.setProxyConfig(proxyConfig.getProxyConfig().get());
		}		
		httpClient = httpService.getClientFactory()
				.create(clientConfigurationBuilder.build());
		httpClient.start();
		
	}


  @Override
  public void disconnect(ChucknorrisConnection connection) {
    try {
      connection.invalidate();
    } catch (Exception e) {
      LOGGER.error("Error while disconnecting [" + connection.getHostname() + "]: " + e.getMessage(), e);
    }
  }


}
