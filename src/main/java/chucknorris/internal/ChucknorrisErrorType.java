package chucknorris.internal;

import org.mule.runtime.extension.api.error.ErrorTypeDefinition;
import org.mule.runtime.extension.api.error.MuleErrors;

import java.util.Optional;

public enum ChucknorrisErrorType implements ErrorTypeDefinition<ChucknorrisErrorType> {
    CONNECTIVITY(MuleErrors.CONNECTIVITY),
    UNSUPPORTED_CONTENT_TYPE(MuleErrors.VALIDATION),
    UNSUPPORTED_ENCODING_TYPE(MuleErrors.VALIDATION),
    TIMEOUT;

    private ErrorTypeDefinition<? extends Enum<?>> parent;

    ChucknorrisErrorType(ErrorTypeDefinition<? extends Enum<?>> parent) {
        this.parent = parent;
    }

    ChucknorrisErrorType() {
    }

    @Override
    public Optional<ErrorTypeDefinition<? extends Enum<?>>> getParent() {
        return Optional.ofNullable(parent);
    }

}
