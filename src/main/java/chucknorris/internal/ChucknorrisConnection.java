package chucknorris.internal;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;

import org.apache.http.client.utils.URIBuilder;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.http.api.HttpConstants.Method;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.domain.entity.HttpEntity;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.request.HttpRequestBuilder;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents an extension connection just as example (there is no
 * real connection with anything here c:).
 */
public final class ChucknorrisConnection {

	private final Logger LOGGER = LoggerFactory.getLogger(ChucknorrisConnection.class);

	private final String hostname;

	private HttpClient httpClient;

	public ChucknorrisConnection(String hostname, HttpClient httpClient) {
		this.hostname = hostname;
		this.httpClient = httpClient;
	}

	public String executeGetRequest(String path, int ttl) throws IOException, TimeoutException, ConnectionException {
		HttpRequestBuilder builder = HttpRequest.builder();
		HttpRequest httpRequest = builder.method(Method.GET).uri(buildURI(path)).build();

		HttpResponse response;
		response = httpClient.send(httpRequest, ttl, true, null);
		if ((response.getStatusCode() != 200) && (response.getStatusCode() != 201))
			throw new ConnectionException("Can't connect to " + hostname);

		String ret = convertToString(response.getEntity());
		return ret;
	}

	public CompletableFuture<HttpResponse> executeAsyncGetRequest(String path, int ttl) {
		HttpRequestBuilder builder = HttpRequest.builder();
		HttpRequest httpRequest = builder.method(Method.GET).uri(buildURI(path)).build();
		CompletableFuture<HttpResponse> response = httpClient.sendAsync(httpRequest, ttl, true, null);
		return response;
	}

	private URI buildURI(String path) {
		try {
			URI ret = new URIBuilder().setScheme("https").setHost(hostname).setPath(path).build();
			LOGGER.info("URI chucknorris: " + ret.toString());
			return ret;
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}

	}

	private String convertToString(HttpEntity entity) {
		try {
			return new String(entity.getBytes());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	public boolean isValid() {
		return (httpClient != null);
	}

	public void invalidate() {
		httpClient = null;
	}

	public String getHostname() {
		return hostname;
	}

}
