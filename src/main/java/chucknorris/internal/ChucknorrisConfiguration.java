package chucknorris.internal;

import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.connectivity.ConnectionProviders;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;

/**
 * This class represents an extension configuration, values set in this class
 * are commonly used across multiple operations since they represent something
 * core from the extension.
 */
@Operations(ChucknorrisOperations.class)
@ConnectionProviders(ChucknorrisConnectionProvider.class)
public class ChucknorrisConfiguration {
	
	
    @Parameter
    @Optional(defaultValue = "0")
    private int ttl;

	public int getTtl() {
		return ttl;
	} 

	

}
