package chucknorris.internal;

import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.api.meta.Category;
import org.mule.runtime.extension.api.annotation.Configurations;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;
import org.mule.runtime.extension.api.annotation.error.ErrorTypes;
import org.mule.runtime.extension.api.annotation.error.Throws;


/**
 * This is the main class of an extension, is the entry point from which configurations, connection providers, operations
 * and sources are going to be declared.
 */
@Xml(prefix = "chucknorris")
@Extension(name = "Chucknorris", vendor = "german.leotta@hotovo.com", category = Category.COMMUNITY)
@Configurations(ChucknorrisConfiguration.class)
@ErrorTypes(ChucknorrisErrorType.class)
@Throws(RandomOperationErrorTypeProvider.class)
public class ChucknorrisExtension {

}
