package chucknorris.internal;

import static org.mule.runtime.extension.api.annotation.param.MediaType.ANY;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.param.Config;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.exception.ModuleException;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.extension.api.runtime.process.CompletionCallback;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * This class is a container for operations, every public method in this class
 * will be taken as an extension operation.
 */
public class ChucknorrisOperations {

	private final Logger LOGGER = LoggerFactory.getLogger(ChucknorrisOperations.class);

	/**
	 * Example of an operation that uses the configuration and a connection instance
	 * to perform some action.
	 */
	@MediaType(value = ANY, strict = false)
    @Throws(RandomOperationErrorTypeProvider.class)
	public String random(@Config ChucknorrisConfiguration  config, @Connection ChucknorrisConnection connection) {

		try {
			String result = connection.executeGetRequest("jokes/random", config.getTtl());
			LOGGER.info("jokes/random", result);
			return result;
		} catch (ConnectionException e) {
			LOGGER.error("Error getting content",e);
            throw new ModuleException(ChucknorrisErrorType.UNSUPPORTED_ENCODING_TYPE, e);
		} catch (IOException e) {
			LOGGER.error("Error on connection",e);
            throw new ModuleException(ChucknorrisErrorType.CONNECTIVITY, e);
		} catch (TimeoutException e) {
			LOGGER.error("Error timeout expired",e);
            throw new ModuleException(ChucknorrisErrorType.TIMEOUT, e);
		}
	}

	@MediaType(value = ANY, strict = false)
	public void randomAsync(@Config ChucknorrisConfiguration  config, @Connection ChucknorrisConnection connection, CompletionCallback<String, Void> callback) {

		HttpResponse response = null;
		try {

			response = connection.executeAsyncGetRequest("jokes/random", config.getTtl()).get();
			if (response.getStatusCode() == 200) {
				callback.success(
						Result.<String, Void>builder().output(new String(response.getEntity().getBytes())).build());
				return;
			}

		} catch (Exception e) {
			LOGGER.error("Error on randomAsync",e);
			callback.error(e);
		} 
	}

}
