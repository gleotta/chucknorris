# Chucknorris Extension

This is a simple "Hello World" Mule connector project using HttpClient in order to get random jokes (Chuck Norris tips) from the api https://api.chucknorris.io/jokes/random

An example of response is
 

	{
	"icon_url" : "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
	"id" : "HZSyXy5GS6i5dVtsNA9KWA",
	"url" : "",
	"value" : "When God said "let there be light", he first asked Chuck Norris if it was ok..."
	}


This connector has two operation in order to get a random joke, the first one is blocking and the another one is non-blocking. 



	public class ChucknorrisOperations {
	
		..
		
		/**
		 * Example of an operation that uses the configuration and a connection instance
		 * to perform some action.
		 */
		@MediaType(value = ANY, strict = false)
	    @Throws(RandomOperationErrorTypeProvider.class)
		public String random(@Config ChucknorrisConfiguration  config, @Connection ChucknorrisConnection connection) {
	
			try {
				String result = connection.executeGetRequest("jokes/random", config.getTtl());
				LOGGER.info("jokes/random", result);
				return result;
			} catch (ConnectionException e) {
				LOGGER.error("Error getting content",e);
	            throw new ModuleException(ChucknorrisErrorType.UNSUPPORTED_ENCODING_TYPE, e);
			} catch (IOException e) {
				LOGGER.error("Error on connection",e);
	            throw new ModuleException(ChucknorrisErrorType.CONNECTIVITY, e);
			} catch (TimeoutException e) {
				LOGGER.error("Error timeout expired",e);
	            throw new ModuleException(ChucknorrisErrorType.TIMEOUT, e);
			}
		}
	
		@MediaType(value = ANY, strict = false)
		public void randomAsync(@Config ChucknorrisConfiguration  config, @Connection ChucknorrisConnection connection, CompletionCallback<String, Void> callback) {
	
			HttpResponse response = null;
			try {
	
				response = connection.executeAsyncGetRequest("jokes/random", config.getTtl()).get();
				if (response.getStatusCode() == 200) {
					callback.success(
							Result.<String, Void>builder().output(new String(response.getEntity().getBytes())).build());
					return;
				}
	
			} catch (Exception e) {
				LOGGER.error("Error on randomAsync",e);
				callback.error(e);
			} 
		}
	
	}

	

Add this dependency to your application pom.xml

```
<groupId>hotovo</groupId>
<artifactId>chucknorris</artifactId>
<version>1.0.0-SNAPSHOT</version>
<classifier>mule-plugin</classifier>
```
